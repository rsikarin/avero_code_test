### Assumptions
- labor entries are collapsed on clock_in timestamp when using day, week, or month
- the timestamps of checks and labor entries are always aligned (I noticed that is not always the case)
- the pay rate is hourly in labor entries
- cost and price in ordered items are the true cost and price
- users are okay with UTC timestamps and all truncating/rounding are done in UTC without regard for locale
- all API requests use correct and existent business IDs, date ranges, time intervals, etc.

### Design Decisions
- ingest source API PoS data just once
    - ingest all data at once and store in local DB (Mongo)
- use simple mongo queries to retrieve data
    - group and aggregate using services to maintain control and flexibility over future feature requests

### Instructions
- this application requires >= Java 8 to run (I'm using 1.8.0_112)
- download and install MongoDB 3.4.0
- launch mongo with __mongod__ command
    - this will launch mongo without authentication
    - data will be stored in the default spring boot database __test__
- repalce placeholder __token__ with real API token in __application.yml__ file
- build and run the project by invoking the Gradle wrapper: __gradlew run__ or __gradlew.bat run__ depending on your system
    - these are wrappers for gradle and exist so users do not need to have gradle installed
    - this will build and run an executable __JAR__
- the API is now running on localhost:8080 and has two endpoints
    - __/ingest__ triggers ingestion from data source api and stores the data into your local mongo
        - this process can take some time (less than a minute) and will return a string to the requester once complete
    - __/reporting...__ handles all of the analytics 