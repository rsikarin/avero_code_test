package com.avero.util;

import com.avero.model.Groupable;
import com.avero.reports.TimeFrame;

import java.time.DayOfWeek;
import java.time.Instant;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAdjusters;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class DateUtil {

    public static Date isoStringToDate(String isoString) {
        ZonedDateTime zonedDateTime = ZonedDateTime.parse(isoString);
        return Date.from(zonedDateTime.toInstant());
    }

    public static <T extends Groupable> Map<TimeFrame, List<T>> groupByTimeInterval(String timeInterval, List<T> elements) {
        Map<TimeFrame, List<T>> output = new HashMap<>();
        ChronoUnit chronoUnit;

        switch (timeInterval) {
            case "hour":
                chronoUnit = ChronoUnit.HOURS;
                break;
            case "day":
                chronoUnit = ChronoUnit.DAYS;
                break;
            case "week":
                chronoUnit = ChronoUnit.WEEKS;
                break;
            case "month":
                chronoUnit = ChronoUnit.MONTHS;
                break;
            default:
                throw new RuntimeException("Please select hour|day|week|month");
        }

        for (T element : elements) {
            TimeFrame elTimeFrame = element.getGroupingTimeFrame(chronoUnit);

            if (output.containsKey(elTimeFrame)) {
                List<T> keyElements = output.get(elTimeFrame);
                keyElements.add(element);
            } else {
                output.put(
                    elTimeFrame,
                    Stream.of(element).collect(Collectors.toList())
                );
            }
        }

        return output;
    }

    public static Instant truncateDate(Instant instant, ChronoUnit chronoUnit) {
        switch (chronoUnit) {
            case HOURS:
            case DAYS:
                return instant.truncatedTo(chronoUnit);
            case WEEKS:
                return truncateToWeek(instant);
            case MONTHS:
                return truncateToMonth(instant);
            default:
                throw new RuntimeException("Unsupported time interval: " + chronoUnit);
        }
    }

    private static Instant truncateToMonth(Instant instant) {
        ZonedDateTime zonedDateTime = ZonedDateTime.ofInstant(instant, ZoneOffset.UTC).truncatedTo(ChronoUnit.DAYS);
        zonedDateTime = zonedDateTime.withDayOfMonth(1);

        return zonedDateTime.toInstant();
    }

    private static Instant truncateToWeek(Instant instant) {
        ZonedDateTime zonedDateTime = ZonedDateTime.ofInstant(instant, ZoneOffset.UTC).truncatedTo(ChronoUnit.DAYS);
        zonedDateTime = zonedDateTime.with(TemporalAdjusters.previousOrSame(DayOfWeek.SUNDAY));

        return zonedDateTime.toInstant();
    }

    public static Instant incrementDate(Instant instant, ChronoUnit chronoUnit) {
        switch (chronoUnit) {
            case HOURS:
            case DAYS:
                return instant.plus(1, chronoUnit);
            case WEEKS:
                return ZonedDateTime.ofInstant(instant, ZoneOffset.UTC).plusWeeks(1).toInstant();
            case MONTHS:
                return ZonedDateTime.ofInstant(instant, ZoneOffset.UTC).plusMonths(1).toInstant();
            default:
                throw new RuntimeException("Unsupported time interval: " + chronoUnit);
        }
    }
}
