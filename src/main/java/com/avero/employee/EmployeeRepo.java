package com.avero.employee;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Component;

@Component
public interface EmployeeRepo extends MongoRepository<Employee, String> {
}
