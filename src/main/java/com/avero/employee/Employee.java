package com.avero.employee;

import com.avero.model.Resource;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class Employee extends Resource {

    private String business_id;
    private String first_name;
    private String last_name;
    private int pay_rate;
}
