package com.avero.labor_entry;

import com.avero.ingestion.SourceDataIngestor;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.avero.util.JsonUtil.MAPPER;

@Service
public class LaborEntryService {

    @Autowired
    private LaborEntryRepo laborEntryRepo;

    @Autowired
    private SourceDataIngestor sourceDataIngestor;

    private final static String dataSourcePath = "laborEntries";
    private final static ObjectReader reader = MAPPER.readerFor(new TypeReference<List<LaborEntry>>() {
    });

    public void fillLaborEntryRepo() {
        int responseCount = 0;
        int offset = 0;

        do {
            JsonNode jsonNode = sourceDataIngestor.getJsonForEntity(dataSourcePath, sourceDataIngestor.MAX_LIMIT, offset);

            try {
                List<LaborEntry> laborEntries = reader.readValue(jsonNode);
                responseCount = laborEntries.size();
                laborEntryRepo.saveAll(laborEntries);
                offset += sourceDataIngestor.MAX_LIMIT;
            } catch (Exception e) {
                System.err.println("Problem deserializing JSON: " + e.getMessage());
            }
        } while (responseCount > 0);
    }

    public void emptyLaborEntryRepo() {
        laborEntryRepo.deleteAll();
    }

    public List<LaborEntry> getLaborEntriesBetween(String businessId, Date start, Date end) {
        return laborEntryRepo.findLaborEntriesBetween(businessId, start, end);
    }

    public List<ExpandedLaborEntry> expandLaborEntriesByHour(List<LaborEntry> laborEntries) {
        List<ExpandedLaborEntry> expandedLaborEntries = new ArrayList<>();

        for (LaborEntry laborEntry : laborEntries) {
            Instant start = laborEntry.getClock_in().toInstant().truncatedTo(ChronoUnit.HOURS);
            Instant end = laborEntry.getClock_out().toInstant().truncatedTo(ChronoUnit.HOURS);

            while (start.isBefore(end)) {
                expandedLaborEntries.add(
                    new ExpandedLaborEntry(
                        start,
                        laborEntry.getPay_rate()
                    )
                );

                start = start.plus(1, ChronoUnit.HOURS);
            }
        }

        return expandedLaborEntries;
    }

    public List<ExpandedLaborEntry> collapseLaborEntries(List<LaborEntry> laborEntries, ChronoUnit chronoUnit) {
        List<ExpandedLaborEntry> expandedLaborEntries = new ArrayList<>();

        for (LaborEntry laborEntry : laborEntries) {
            long hours = getNumHours(laborEntry);
            int fullPay = (int) hours * laborEntry.getPay_rate();

            expandedLaborEntries.add(
                new ExpandedLaborEntry(
                    // assumption: collapsing on clock in instead of expanding days/weeks/months
                    laborEntry.getClock_in().toInstant().truncatedTo(chronoUnit),
                    fullPay
                )
            );
        }

        return expandedLaborEntries;
    }

    private long getNumHours(LaborEntry laborEntry) {
        Instant start = laborEntry.getClock_in().toInstant().truncatedTo(ChronoUnit.HOURS);
        Instant end = laborEntry.getClock_out().toInstant().truncatedTo(ChronoUnit.HOURS);

        return start.until(end, ChronoUnit.HOURS);
    }

    public int sumLaborEntries(List<ExpandedLaborEntry> expandedLaborEntries) {
        return expandedLaborEntries.stream().mapToInt(ExpandedLaborEntry::getFullPay).sum();
    }
}
