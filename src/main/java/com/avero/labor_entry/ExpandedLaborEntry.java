package com.avero.labor_entry;

import com.avero.model.Groupable;
import com.avero.reports.TimeFrame;
import com.avero.util.DateUtil;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.Instant;
import java.time.temporal.ChronoUnit;

@Data
@AllArgsConstructor
public class ExpandedLaborEntry implements Groupable {

    private Instant dateTime;
    private int fullPay;

    @Override
    public TimeFrame getGroupingTimeFrame(ChronoUnit chronoUnit) {
        Instant start = DateUtil.truncateDate(dateTime, chronoUnit);

        return new TimeFrame(
            start,
            DateUtil.incrementDate(start, chronoUnit)
        );
    }
}
