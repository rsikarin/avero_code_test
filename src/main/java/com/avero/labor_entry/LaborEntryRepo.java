package com.avero.labor_entry;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

@Component
public interface LaborEntryRepo extends MongoRepository<LaborEntry, String> {

    @Query("{'business_id' : ?0, 'clock_out' : {$gte : ?1}, 'clock_in' : {$lt: ?2} }")
    List<LaborEntry> findLaborEntriesBetween(String businessId, Date start, Date end);
}
