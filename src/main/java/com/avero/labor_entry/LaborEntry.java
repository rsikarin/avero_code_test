package com.avero.labor_entry;

import com.avero.model.Resource;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import org.springframework.data.mongodb.core.index.Indexed;

import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class LaborEntry extends Resource {

    @Indexed
    private String business_id;

    private String employee_id;
    private String name;

    @Indexed
    private Date clock_in;

    @Indexed
    private Date clock_out;
    private int pay_rate;
}
