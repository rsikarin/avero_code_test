package com.avero.ordered_item;

import com.avero.ingestion.SourceDataIngestor;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.avero.util.JsonUtil.MAPPER;

@Service
public class OrderedItemService {

    @Autowired
    private OrderedItemRepo orderedItemRepo;

    @Autowired
    private SourceDataIngestor sourceDataIngestor;

    private final static String dataSourcePath = "orderedItems";
    private final static ObjectReader reader = MAPPER.readerFor(new TypeReference<List<OrderedItem>>() {});

    public void fillOrderedItemRepo() {
        int responseCount = 0;
        int offset = 0;

        do {
            JsonNode jsonNode = sourceDataIngestor.getJsonForEntity(dataSourcePath, sourceDataIngestor.MAX_LIMIT, offset);

            try {
                List<OrderedItem> orderedItems = reader.readValue(jsonNode);
                responseCount = orderedItems.size();
                orderedItemRepo.saveAll(orderedItems);
                offset += sourceDataIngestor.MAX_LIMIT;
            } catch (Exception e) {
                System.err.println("Problem deserializing JSON: " + e.getMessage());
            }
        } while (responseCount > 0);
    }

    public void emptyOrderedItemRepo() {
        orderedItemRepo.deleteAll();
    }

    public List<OrderedItem> findOrderedItemsByCheckIds(List<String> checkIds) {
        return orderedItemRepo.findByCheckIdIn(checkIds);
    }

    public int sumPrices(List<OrderedItem> orderedItems) {
        return orderedItems.stream().mapToInt(OrderedItem::getPrice).sum();
    }

    public int sumCosts(List<OrderedItem> orderedItems) {
        return orderedItems.stream().mapToInt(OrderedItem::getCost).sum();
    }
}
