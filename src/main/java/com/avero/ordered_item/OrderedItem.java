package com.avero.ordered_item;

import com.avero.model.Resource;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import org.springframework.data.mongodb.core.index.Indexed;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class OrderedItem extends Resource {

    private String business_id;
    private String employee_id;

    @Indexed
    private String check_id;

    private String item_id;
    private String name;
    private int cost;
    private int price;
    private boolean voided;
}
