package com.avero.ordered_item;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface OrderedItemRepo extends MongoRepository<OrderedItem, String> {

    @Query("{'check_id' : {$in: ?0} }")
    List<OrderedItem> findByCheckIdIn(List<String> checkIds);
}
