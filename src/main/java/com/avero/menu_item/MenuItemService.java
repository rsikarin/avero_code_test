package com.avero.menu_item;

import com.avero.ingestion.SourceDataIngestor;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.avero.util.JsonUtil.MAPPER;

@Service
public class MenuItemService {

    @Autowired
    private MenuItemRepo menuItemRepo;

    @Autowired
    private SourceDataIngestor sourceDataIngestor;

    private final static String dataSourcePath = "menuItems";
    private final static ObjectReader reader = MAPPER.readerFor(new TypeReference<List<MenuItem>>() {});

    public void fillMenuItemRepo() {
        int responseCount = 0;
        int offset = 0;

        do {
            JsonNode jsonNode = sourceDataIngestor.getJsonForEntity(dataSourcePath, sourceDataIngestor.MAX_LIMIT, offset);

            try {
                List<MenuItem> menuItems = reader.readValue(jsonNode);
                responseCount = menuItems.size();
                menuItemRepo.saveAll(menuItems);
                offset += sourceDataIngestor.MAX_LIMIT;
            } catch (Exception e) {
                System.err.println("Problem deserializing JSON: " + e.getMessage());
            }
        } while (responseCount > 0);
    }

    public void emptyMenuItemRepo() {
        menuItemRepo.deleteAll();
    }
}
