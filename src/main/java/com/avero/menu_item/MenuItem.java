package com.avero.menu_item;

import com.avero.model.Resource;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class MenuItem extends Resource {

    private String business_id;
    private String name;
    private int cost;
    private int price;
}
