package com.avero.menu_item;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Component;

@Component
public interface MenuItemRepo extends MongoRepository<MenuItem, String> {

}
