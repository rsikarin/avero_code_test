package com.avero.check;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

@Component
public interface CheckRepo extends MongoRepository<Check, String> {

    @Query("{'closed_at' : {$gte: ?0, $lt: ?1}, 'closed' : true, 'business_id' : ?2 }")
    List<Check> findChecksWithClosedAtBetween(Date start, Date end, String businessId);
}
