package com.avero.check;

import com.avero.model.Groupable;
import com.avero.model.Resource;
import com.avero.reports.TimeFrame;
import com.avero.util.DateUtil;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import org.springframework.data.mongodb.core.index.Indexed;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class Check extends Resource implements Groupable {

    @Indexed
    private String business_id;
    private String employee_id;
    private String name;
    private boolean closed;
    private Date closed_at;

    @Override
    public TimeFrame getGroupingTimeFrame(ChronoUnit chronoUnit) {
        Instant start = DateUtil.truncateDate(closed_at.toInstant(), chronoUnit);

        return new TimeFrame(
            start,
            DateUtil.incrementDate(start, chronoUnit)
        );
    }
}
