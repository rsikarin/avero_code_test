package com.avero.check;

import com.avero.ingestion.SourceDataIngestor;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.avero.util.JsonUtil.MAPPER;

@Service
public class CheckService {

    @Autowired
    private CheckRepo checkRepo;

    @Autowired
    private SourceDataIngestor sourceDataIngestor;

    private final static String dataSourcePath = "checks";
    private final static ObjectReader reader = MAPPER.readerFor(new TypeReference<List<Check>>() {
    });

    public void fillCheckRepo() {
        int responseCount = 0;
        int offset = 0;

        do {
            JsonNode jsonNode = sourceDataIngestor.getJsonForEntity(dataSourcePath, sourceDataIngestor.MAX_LIMIT, offset);

            try {
                List<Check> checks = reader.readValue(jsonNode);
                responseCount = checks.size();
                checkRepo.saveAll(checks);
                offset += sourceDataIngestor.MAX_LIMIT;
            } catch (Exception e) {
                System.err.println("Problem deserializing JSON: " + e.getMessage());
            }
        } while (responseCount > 0);
    }

    public void emptyCheckRepo() {
        checkRepo.deleteAll();
    }

    public List<Check> findChecksWithClosedAtDateBetween(Date start, Date end, String businessId) {
        return checkRepo.findChecksWithClosedAtBetween(start, end, businessId);
    }

    public Map<String, List<Check>> groupChecksByEmployee(List<Check> checks) {
        return checks.stream().collect(Collectors.groupingBy(Check::getEmployee_id));
    }
}
