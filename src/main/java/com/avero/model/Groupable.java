package com.avero.model;

import com.avero.reports.TimeFrame;

import java.time.temporal.ChronoUnit;

public interface Groupable {

    TimeFrame getGroupingTimeFrame(ChronoUnit chronoUnit);
}
