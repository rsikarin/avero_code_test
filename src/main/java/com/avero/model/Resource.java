package com.avero.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import org.springframework.data.annotation.Id;

import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class Resource {

    @Id
    private String id;

    private Date created_at;
    private Date updated_at;
}
