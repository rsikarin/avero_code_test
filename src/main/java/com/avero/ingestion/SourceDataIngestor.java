package com.avero.ingestion;

import com.avero.business.BusinessService;
import com.avero.check.CheckService;
import com.avero.employee.EmployeeService;
import com.avero.labor_entry.LaborEntryService;
import com.avero.menu_item.MenuItemService;
import com.avero.ordered_item.OrderedItemService;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;

import static com.avero.util.JsonUtil.MAPPER;

@Component
public class SourceDataIngestor {

    @Value("${datasource-api.max-limit}")
    public int MAX_LIMIT;

    private final static RestTemplate REST_TEMPLATE = new RestTemplate();
    private static HttpEntity<String> REQUEST;

    @Value("${datasource-api.base-url}")
    private String datasource_api_base_url;

    @Value("${datasource-api.token}")
    private String datasource_api_token;

    @Value("${datasource-api.data-field}")
    private String datasource_api_data_field;

    @Autowired
    private BusinessService businessService;

    @Autowired
    private CheckService checkService;

    @Autowired
    private EmployeeService employeeService;

    @Autowired
    private LaborEntryService laborEntryService;

    @Autowired
    private MenuItemService menuItemService;

    @Autowired
    private OrderedItemService orderedItemService;

    @PostConstruct
    public void init() {
        initRequestWithHeaders();
    }

    public String ingestSourceData() {

        /*
         *  Fill repositories using source data api
         */
        businessService.emptyBusinessRepo();
        businessService.fillBusinessRepo();

        checkService.emptyCheckRepo();
        checkService.fillCheckRepo();

        employeeService.emptyEmployeeRepo();
        employeeService.fillEmployeeRepo();

        laborEntryService.emptyLaborEntryRepo();
        laborEntryService.fillLaborEntryRepo();

        menuItemService.emptyMenuItemRepo();
        menuItemService.fillMenuItemRepo();

        orderedItemService.emptyOrderedItemRepo();
        orderedItemService.fillOrderedItemRepo();

        return "source data transferred into mongo";
    }

    private void initRequestWithHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", datasource_api_token);
        REQUEST = new HttpEntity<>(headers);
    }

    public JsonNode getJsonForEntity(String entity, int limit, int offset) {
        JsonNode output = null;

        String requestUrl = getUrl(entity, limit, offset);
        ResponseEntity<String> response = getResponseFromSourceDataApi(requestUrl);

        try {
            ObjectNode node = MAPPER.readValue(response.getBody(), ObjectNode.class);

            if (node.has(datasource_api_data_field)) {
                output = node.get(datasource_api_data_field);
            }
        } catch (Exception e) {
            System.err.println("Error reading response body: " + e.getMessage());
        }

        return output;

    }

    private ResponseEntity<String> getResponseFromSourceDataApi(String url) {
        return REST_TEMPLATE.exchange(
                url,
                HttpMethod.GET,
                REQUEST,
                String.class);
    }

    private String getUrl(String entity, int limit, int offset) {
        return datasource_api_base_url + entity + "?limit=" + limit + "&offset=" + offset;
    }
}
