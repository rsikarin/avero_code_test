package com.avero.ingestion;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class IngestionController {

    @Autowired
    private SourceDataIngestor sourceDataIngestor;

    @RequestMapping("/ingest")
    public String ingest() {
        return sourceDataIngestor.ingestSourceData();
    }
}
