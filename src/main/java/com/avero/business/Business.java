package com.avero.business;

import com.avero.model.Resource;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class Business extends Resource {

    private String name;
    private List<Integer> hours;
}
