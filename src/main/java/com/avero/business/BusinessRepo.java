package com.avero.business;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Component;

@Component
public interface BusinessRepo extends MongoRepository<Business, String> {

}
