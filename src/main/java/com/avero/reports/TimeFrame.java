package com.avero.reports;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.Instant;

@Data
@AllArgsConstructor
public class TimeFrame {

    private Instant start;
    private Instant end;
}
