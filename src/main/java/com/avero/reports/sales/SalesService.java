package com.avero.reports.sales;

import com.avero.check.Check;
import com.avero.check.CheckService;
import com.avero.ordered_item.OrderedItem;
import com.avero.ordered_item.OrderedItemService;
import com.avero.reports.TimeFrame;
import com.avero.util.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class SalesService {

    @Autowired
    private CheckService checkService;

    @Autowired
    private OrderedItemService orderedItemService;

    public Map<TimeFrame, List<OrderedItem>> getOrderedItemsByTimeInterval(Date startDate, Date endDate, String businessId, String timeInterval) {
        List<Check> checksInDateRange = checkService.findChecksWithClosedAtDateBetween(startDate, endDate, businessId);

        return getOrderedItemsByTimeInterval(timeInterval, checksInDateRange);
    }

    public Map<TimeFrame, List<OrderedItem>> getOrderedItemsByTimeInterval(String timeInterval, List<Check> checksInDateRange) {
        Map<TimeFrame, List<OrderedItem>> output = new HashMap<>();

        Map<TimeFrame, List<Check>> checksByTimeInterval = DateUtil.groupByTimeInterval(timeInterval, checksInDateRange);

        checksByTimeInterval.forEach((keyTimeInterval, checks) -> {
            List<String> checkIds = checks.stream().map(Check::getId).collect(Collectors.toList());
            List<OrderedItem> orderedItems = orderedItemService.findOrderedItemsByCheckIds(checkIds);

            output.put(keyTimeInterval, orderedItems);
        });

        return output;
    }
}
