package com.avero.reports;

import com.avero.reports.egs.EgsService;
import com.avero.reports.fcp.FcpService;
import com.avero.reports.lcp.LcpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ReportingController {

    @Autowired
    private LcpService lcpService;

    @Autowired
    private FcpService fcpService;

    @Autowired
    private EgsService egsService;

    @RequestMapping("/reporting")
    public Report report(
            @RequestParam(value = "business_id") String businessId,
            @RequestParam(value = "report") String reportType,
            @RequestParam(value = "timeInterval") String timeInterval,
            @RequestParam(value = "start") String start,
            @RequestParam(value = "end") String end
    ) {
        Report report = null;

        switch (reportType) {
            case "LCP":
                report = lcpService.getLcpReport(businessId, timeInterval, start, end);
                break;
            case "FCP":
                report = fcpService.getFcpReport(businessId, timeInterval, start, end);
                break;
            case "EGS":
                report = egsService.getEgsReport(businessId, timeInterval, start, end);
                break;
            default:
                System.err.println("Please select a valid report type");
        }

        return report;
    }
}
