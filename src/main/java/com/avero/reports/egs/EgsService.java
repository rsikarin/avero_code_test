package com.avero.reports.egs;

import com.avero.check.Check;
import com.avero.check.CheckService;
import com.avero.ordered_item.OrderedItem;
import com.avero.ordered_item.OrderedItemService;
import com.avero.reports.Report;
import com.avero.reports.TimeFrame;
import com.avero.reports.sales.SalesService;
import com.avero.util.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class EgsService {

    @Autowired
    private CheckService checkService;

    @Autowired
    private SalesService salesService;

    @Autowired
    private OrderedItemService orderedItemService;

    public Report getEgsReport(String businessId, String timeInterval, String start, String end) {
        Report report = new Report(
            "EGS",
            timeInterval,
            new ArrayList<>()
        );

        Date startDate = DateUtil.isoStringToDate(start);
        Date endDate = DateUtil.isoStringToDate(end);

        List<Check> checksInDateRange = checkService.findChecksWithClosedAtDateBetween(startDate, endDate, businessId);

        Map<String, List<Check>> checksByEmployeeId = checkService.groupChecksByEmployee(checksInDateRange);

        checksByEmployeeId.forEach((employeeId, checkList) -> {
            String employeeName = checkList.get(0).getName();

            // Sales data lookup
            Map<TimeFrame, List<OrderedItem>> orderedItemsByTimeInterval = salesService.getOrderedItemsByTimeInterval(timeInterval, checkList);

            List<EgsReportData> egsReportDataList = getReportData(employeeName, orderedItemsByTimeInterval);
            report.getReportData().addAll(egsReportDataList);
        });

        return report;
    }

    private List<EgsReportData> getReportData(String employeeName, Map<TimeFrame, List<OrderedItem>> orderedItemsByTimeInterval) {
        List<EgsReportData> egsReportDataList = new ArrayList<>();

        orderedItemsByTimeInterval.forEach((timeInterval, orderedItems) -> {
            double sales = (double) orderedItemService.sumPrices(orderedItems);

            EgsReportData egsReportData = new EgsReportData(
                timeInterval,
                Math.round(sales * 100.0) / 100.0,
                employeeName
            );

            egsReportDataList.add(egsReportData);
        });

        return egsReportDataList;
    }
}
