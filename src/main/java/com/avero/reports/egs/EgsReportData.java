package com.avero.reports.egs;

import com.avero.reports.ReportData;
import com.avero.reports.TimeFrame;
import lombok.Data;

@Data
public class EgsReportData extends ReportData {

    private String employee;

    public EgsReportData(TimeFrame timeFrame, double value, String employeeName) {
        super(timeFrame, value);
        this.employee = employeeName;
    }
}
