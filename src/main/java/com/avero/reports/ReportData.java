package com.avero.reports;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ReportData {

    private TimeFrame timeFrame;
    private double value;
}
