package com.avero.reports.fcp;

import com.avero.ordered_item.OrderedItem;
import com.avero.ordered_item.OrderedItemService;
import com.avero.reports.Report;
import com.avero.reports.ReportData;
import com.avero.reports.TimeFrame;
import com.avero.reports.sales.SalesService;
import com.avero.util.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class FcpService {

    @Autowired
    private SalesService salesService;

    @Autowired
    private OrderedItemService orderedItemService;

    public Report getFcpReport(String businessId, String timeInterval, String start, String end) {
        Date startDate = DateUtil.isoStringToDate(start);
        Date endDate = DateUtil.isoStringToDate(end);

        Map<TimeFrame, List<OrderedItem>> orderedItemsByTimeInterval = salesService.getOrderedItemsByTimeInterval(
            startDate,
            endDate,
            businessId,
            timeInterval
        );

        List<ReportData> reportDataList = getReportData(orderedItemsByTimeInterval);

        return new Report(
            "FCP",
            timeInterval,
            reportDataList
        );
    }

    private List<ReportData> getReportData(Map<TimeFrame, List<OrderedItem>> orderedItemsByTimeInterval) {
        List<ReportData> reportDataList = new ArrayList<>();

        orderedItemsByTimeInterval.forEach((timeInterval, orderedItems) -> {

            double sales = (double) orderedItemService.sumPrices(orderedItems);
            double costs = (double) orderedItemService.sumCosts(orderedItems);


            double result = costs / sales;
            ReportData reportData = new ReportData(
                timeInterval,
                Math.round(result * 100.0) / 100.0
            );

            reportDataList.add(reportData);

        });

        return reportDataList;
    }
}
