package com.avero.reports.lcp;

import com.avero.labor_entry.ExpandedLaborEntry;
import com.avero.labor_entry.LaborEntry;
import com.avero.labor_entry.LaborEntryService;
import com.avero.ordered_item.OrderedItem;
import com.avero.ordered_item.OrderedItemService;
import com.avero.reports.Report;
import com.avero.reports.ReportData;
import com.avero.reports.TimeFrame;
import com.avero.reports.sales.SalesService;
import com.avero.util.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class LcpService {

    @Autowired
    private SalesService salesService;

    @Autowired
    private LaborEntryService laborEntryService;

    @Autowired
    private OrderedItemService orderedItemService;

    public Report getLcpReport(String businessId, String timeInterval, String start, String end) {
        Date startDate = DateUtil.isoStringToDate(start);
        Date endDate = DateUtil.isoStringToDate(end);

        // Sales data lookup
        Map<TimeFrame, List<OrderedItem>> orderedItemsByTimeInterval = salesService.getOrderedItemsByTimeInterval(
            startDate,
            endDate,
            businessId,
            timeInterval
        );

        // labor data lookup
        Map<TimeFrame, List<ExpandedLaborEntry>> laborEntriesByTimeInterval = getLaborEntriesByTimeInterval(
            startDate,
            endDate,
            businessId,
            timeInterval
        );

        List<ReportData> reportDataList = getReportData(orderedItemsByTimeInterval, laborEntriesByTimeInterval);
        return new Report(
            "LCP",
            timeInterval,
            reportDataList
        );
    }

    private Map<TimeFrame, List<ExpandedLaborEntry>> getLaborEntriesByTimeInterval(Date startDate, Date endDate, String businessId, String timeInterval) {
        List<LaborEntry> laborEntries = laborEntryService.getLaborEntriesBetween(businessId, startDate, endDate);

        List<ExpandedLaborEntry> expandedLaborEntries;

        if (timeInterval.equals("hour")) {
            expandedLaborEntries = laborEntryService.expandLaborEntriesByHour(laborEntries);
        } else {
            expandedLaborEntries = laborEntryService.collapseLaborEntries(laborEntries, ChronoUnit.DAYS);
        }

        return DateUtil.groupByTimeInterval(timeInterval, expandedLaborEntries);
    }

    private List<ReportData> getReportData(Map<TimeFrame, List<OrderedItem>> orderedItemsByTimeInterval, Map<TimeFrame, List<ExpandedLaborEntry>> laborEntriesByTimeInterval) {
        List<ReportData> reportDataList = new ArrayList<>();

        orderedItemsByTimeInterval.forEach((timeInterval, orderedItems) -> {

            // only calculating if there is data in both buckets for this time interval
            if (laborEntriesByTimeInterval.containsKey(timeInterval)) {
                double sales = (double) orderedItemService.sumPrices(orderedItems);
                double laborCosts = (double) laborEntryService.sumLaborEntries(
                    laborEntriesByTimeInterval.get(timeInterval)
                );

                double result = laborCosts/sales;
                ReportData reportData = new ReportData(
                    timeInterval,
                    Math.round(result*100.0)/100.0
                );

                reportDataList.add(reportData);
            }
        });

        return reportDataList;
    }
}
