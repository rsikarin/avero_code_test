package com.avero.reports;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class Report {

    private String report;
    private String timeInterval;
    private List<ReportData> reportData;
}
